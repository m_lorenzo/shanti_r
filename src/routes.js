import React from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'


//Provider is a react redux component in charge of passing the store to the sub components in the app
import { Provider } from 'react-redux'

import store , { history } from './store'

import Signup from './Components/Signup/Signup'
import NavbarWrapper from './Components/Navbar/Navbar'


const Home = () => <div><h1>Home</h1></div>


function isAuth() {
 if (localStorage.getItem('token') != null) {
   return true
 } else {
   return false
 }
}




const PrivateRoute = ({ component, ...rest }) =>(
    <Route {...rest} render={props => (
      isAuth() ? (
        React.createElement(component, props)
      ) : (
        <Redirect to={{
          pathname: '/signup',
          state: { from: props.location }
        }}/>
      )
    )}/>
  )





const Routes = () => (
  <Provider store={store}>
    <Router history={history}>
      <div>
        <NavbarWrapper />
        <PrivateRoute exact path="/home" component={Home}/>
        <Route path="/signup/" component={Signup}/>
      </div>
    </Router>
  </Provider>
)

export default Routes
