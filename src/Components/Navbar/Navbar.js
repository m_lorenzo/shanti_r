import React from 'react';
import logo from './images/logo.jpg'

import styled from 'styled-components';

const NavbarWrapper = (props)=> (
  <Navbar>
    <Logo></Logo>
    <LoginDiv>
      <LoginButton>Login</LoginButton>
    </LoginDiv>
  </Navbar>

)


const Logo = styled.div`
  margin:15px 20px;
  float: left;
  width: 50px;
  height: 50px;
  border-radius: 2em;
  background-image: url(${logo});
  background-size: 100%;
`

const LoginDiv = styled.div`
  float: right;
  margin: 20px 40px;
  `

const LoginButton = styled.button`
font-size: inherit;
width: 140px;
border-radius: 30px;
border: 1px solid black;
background-color: ${(props)=> props.color || "grey" };
padding: 10px 30px;
`

const Navbar = styled.div`
  width: 100%;
  height: 70px;
`


export default NavbarWrapper
