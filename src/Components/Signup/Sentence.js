import React from 'react'
import styled from 'styled-components'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actionCreators from '../../actions/index'

import { calcAge } from '../../utils'

import DynamicInput from "./DynamicInput"

class Sentence extends React.Component {

  constructor(props){
    super(props)
    this.handleSignup = this.handleSignup.bind(this)
  }

  handleSignup () {
    const formattedInputs = Object.keys(this.refs)
      .reduce((prev, next) => {
        prev[next] = this.refs[next].state.textValue;
        return prev;
      }, {});
      this.props.makeSignup(formattedInputs, 'basicInfo')
  }


  render () {
    return (
      <Wrapper>
        <Div>
          <p style={{margin: "10px"}}>My name is <DynamicInput type="text" ref="firstName" placeholder="firstname" value={this.props.firstName}/><DynamicInput type="text" ref="lastName" placeholder="lastname" value={this.props.lastName}/></p>
          <p style={{margin: "10px"}}>My email is <DynamicInput type="text" ref="email" placeholder="email" value={this.props.email}/></p>
          <p style={{margin: "10px"}}>I'm a <DynamicInput type="text" ref="gender" placeholder="sex" value={this.props.gender}/> and I'm <DynamicInput type="text" ref="age" placeholder="age" value={this.props.age}/> years old</p>
          <p style={{margin: "10px"}}>I work as <DynamicInput type="text" ref="profession" placeholder="profession" value={this.props.profession}/> and I have a degree in <DynamicInput type="text" ref="education" placeholder="education" value={this.props.education} /></p>
          <p style={{margin: "10px"}}>At the moment I live in <DynamicInput type="text" ref="location" placeholder="city" value={this.props.location}/></p>
        <Button onClick={this.handleSignup}>Next</Button>
      </Div>
      </Wrapper>
    )
  }
}





function mapStateToProps(state){ //also props can be added as a second param
  let fbInfo = state.makeSignup.user.basicInfo
  if (fbInfo) {
    return {
        firstName: fbInfo.first_name || "",
        lastName: fbInfo.last_name || "",
        email: fbInfo.email || "",
        gender: fbInfo.gender || "",
        age: calcAge(fbInfo.birthday) || "" || "",
        education: (fbInfo.education && fbInfo.education[fbInfo.education.length - 1].concentration[0].name) || "",
        location: (fbInfo.location && fbInfo.location.name) || ""
    }
  } else {
    return {}
  }
}


function mapDispachToProps(dispatch){
  return bindActionCreators({ ...actionCreators}, dispatch)
}




export default connect(mapStateToProps, mapDispachToProps)(Sentence);






//same as the wrapper in signup -> has tob shared
const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  overflow: hidden;
`

const Div = styled.div`
  height: 450px;
  width: 45%;
  margin: 10% auto;
  lineHeight: 1.6;
  fontSize: 1.8em;
`



const Button = styled.button`
  font-size: 0.6em;
  width: 340px;
  border-radius: 30px;
  border: 1px solid black;
  background-color: ${(props)=> props.color || "grey" };
  padding: 10px 30px;
  margin-bottom: 25px;
  position: absolute;
  right: 100px;
`
