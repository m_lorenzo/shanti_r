import React from 'react';

//redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actionCreators from '../../actions/index'

//components
import Landing from './Landing'
import Sentence from './Sentence'
import { LivedCountries, VisitedCountries } from "./Countries"
import { Music, Sports, Cuisines, TravelTypes, Interests } from './ButtonSelect'
import {Tip1, Tip2, Tip3 } from './Tip'

const Home = ()=> <div>Home</div>


class Signup extends React.Component {

  constructor(){
    super()
    this.steps = [Landing, Sentence, LivedCountries, VisitedCountries, Music, Sports, Cuisines, TravelTypes, Interests, Tip1, Tip2, Tip3, Home]
  }


  //signup connected with redux state -> everytime it changes it re-render the signup component
  render () {
    let component = this.steps[this.props.counter.value]
    return (
      <div>
        {React.createElement(component)}
      </div>
    )
  }
}


function mapStateToProps(state){
  return {counter: state.makeSignup.counter}
}


function mapDispachToProps(dispatch){
  return bindActionCreators({ ...actionCreators}, dispatch)
}




export default connect(mapStateToProps, mapDispachToProps)(Signup);
