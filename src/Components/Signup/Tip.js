import React from 'react'
import styled from 'styled-components'
import Autocomplete from 'react-google-autocomplete';
import TagsInput from 'react-tagsinput'
import AutosizeInput from 'react-input-autosize'
import axios from 'axios'

import 'react-tagsinput/react-tagsinput.css' // style for the TagsInput component


//redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actionCreators from '../../actions/index'

/*global google*/

const buttons = {
  cousines: ['mediterranean', 'latin american', 'oriental', 'middle east', 'african', 'europea', 'north american'],
  type: ['restaurant', 'trattoria', 'osteria', 'pizza', 'sushi bar', 'coffee', 'bar', 'wine bar', 'bistrot', 'deli',
  'pub', 'diner', 'self service', 'fast food', 'vegetarian', 'vegan', 'gluten free'],
  features: ['panorama', 'terrace', 'garden', 'sea view', 'romantic', 'relax', 'farmhouse', 'chimnney', 'groups',
  'meetings', 'couples', 'family', 'animal friendly', 'cheap', 'classy', 'accessability', 'reservations', 'wifi', 'take away',
  'delivery', 'parking']
}



class Tip extends React.Component {

  constructor(props){
    super(props)
    this.state = {latLng: undefined, dishes: []}
    this.buttonClick = this.buttonClick.bind(this)
    this.tipBody = {
      'cousines': [],
      'type': [],
      'features': [],
      'dishes':[]
    }
  }


  componentWillMount(){
    var that = this
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': this.props.location}, function(results, status) {
      if (status === 'OK') {
        that.setState({latLng: results[0].geometry.location})
      }
    });
  }

  ifAutocompleteResults(){
    if(this.state.autocompleteTip !== undefined){
      return (
        <div>
          <h4>{this.state.autocompleteTip.name}</h4>
          <p>{this.state.autocompleteTip.address}</p>
        </div>
      )
    }
  }

  generateAutocompleteField(){
    return (
      <Autocomplete
        placeholder={'Search for your favourite place'}
        style={{ width: '60%',height: '1.5em'}}
        onPlaceSelected={(place) => {
          this.setState({
            autocompleteTip: {
              name: place.name,
              address: place.formatted_address,
            }
          })
        }}
        types={['establishment']}
        bounds={new google.maps.Circle({center: this.state.latLng, radius: 500}).getBounds()} />

    )
  }

  generateButtonsSection(){
    return (
        Object.keys(buttons).map(function(key){
        return (
            <MainDiv key={key} id={key}>
              <ChildDiv id={key} flexDir={'row'} width={100}>
                {this.createButtons(buttons[key])}
              </ChildDiv>
          </MainDiv>
        )
      }.bind(this))
    )
  }

  createButtons(what){
    return what.map((buttonText, index)=>{
      return <Button onClick={this.buttonClick} key={index} value={buttonText}>{buttonText}</Button>
    })
  }

  buttonClick(event){
    let defStyle = event.target.style
    let infoKind = event.target.parentNode.getAttribute("id")

    if (event.target.className.includes('active') === false) {
      event.target.className += ' active'
      event.target.style.backgroundColor = 'blue'
      event.target.style.color = 'white'
          this.tipBody[infoKind].push(event.target.value)
    } else {
      event.target.className = event.target.className.replace("active", "")
      event.target.style = defStyle
      let index = this.tipBody[infoKind].indexOf(event.target.value)
      this.tipBody[infoKind].splice(index, 1)
    }
  }

  handleChange(dishes){
    this.setState({dishes})
    this.tipBody['dishes'] = this.state.dishes
  }

  submit(){
    let tips;
    this.tipBody['dishes'] = this.state.dishes
    let currentTip = {...this.state.autocompleteTip, ...this.tipBody}
    if (this.props.user.tips === undefined) {
      tips = [currentTip]
    } else {
      this.props.user.tips.push(currentTip)
      tips = this.props.user.tips
    }
    this.props.makeSignup(tips, "tips")
  }

//make the createbutton section a function to be called in a loop

  render () {
    if (this.state.latLng !== undefined) {
      return(
        <Wrapper>
          <MainDiv>
            <ChildDiv flexDir={'row'} width={100}>
              <h1>{this.props.mainText}</h1>
            </ChildDiv>
          </MainDiv>
          <MainDiv>
            <ChildDiv flexDir={'row'} width={100}>
              {this.generateAutocompleteField()}
              {this.ifAutocompleteResults()}
            </ChildDiv>
          </MainDiv>
          {this.generateButtonsSection()}
          <MainDiv id="dishes">
            <ChildDiv>
              <h3>Write your 3 favourite dishes</h3>
                <TagsInput class="tagInput" value={this.state.dishes} onChange={this.handleChange.bind(this)} maxTags={3} renderInput={function autosizingRenderInput ({addTag, ...props}) {
                let {onChange, value, ...other} = props
                return (
                  <AutosizeInput id='small' type='text' onChange={onChange} value={value} {...other} />
                )
              }}/>
            </ChildDiv>
          </MainDiv>
          <MainDiv>
            <Button onClick={this.submit.bind(this)}>Next Tip</Button>
            {/*}<Button onClick={this.registerUser.bind(this)}>Complete Registration</Button>*/}
          </MainDiv>
        </Wrapper>
        )
    } else {
      return null
    }
  }


}


function mapStateToProps(state){
  return {location: state.makeSignup.user.basicInfo.location, user: state.makeSignup.user}
}


function mapDispachToProps(dispatch){
  return bindActionCreators({ ...actionCreators}, dispatch)
}

const HOC = (InnerComponent, mainText)=> class extends React.Component{
  render(){
    return <InnerComponent {...this.props} mainText={mainText}/>
  }
}

export const Tip1 = connect(mapStateToProps, mapDispachToProps)(HOC(Tip, "This is the fun part, suggest your 3 favourite restaurants of your city"))
export const Tip2 = connect(mapStateToProps, mapDispachToProps)(HOC(Tip, "Here we come with the second one"))
export const Tip3 = connect(mapStateToProps, mapDispachToProps)(HOC(Tip, "Last tip to add, make it good!"))


const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`

const MainDiv = styled.div`
  width: 70%;
  margin-bottom: ${(props) => props.marginBottom || 5}%
  text-align: center;
`

const ChildDiv = styled.div`
  width: ${(props)=> props.width}%;
  justify-content: center;
  flex-direction: ${(props)=> props.flexDir || "column"};
  align-items: center;
  text-align: center;
  font-size: ${(props)=> props.fontSize || 1}em;
`

const Button = styled.button`
  font-size: inherit;
  border-radius: 30px;
  border: 1px solid black;
  outline:none;
  background-color: ${(props)=> props.color || "grey" };
  padding: 10px 30px;
  margin-bottom: 20px;
`
