import React from 'react'
import styled from 'styled-components'

class DynamicInput extends React.Component {
  constructor(){
    super()
    this.state = {textValue: ""}
  }

  componentWillMount(){
    if (this.props.value !== undefined) {
      this.setState({textValue: this.props.value})
    }
    this.setState({width:this.calcInputLength()})
  }


  componentDidMount(){
    this.setState({width:this.calcInputLength()})
  }

  calcInputLength(){
    var inputLength;
    if(this.state.textValue !== "" && this.state.textValue.length >= this.props.placeholder.length){
      var valueLength = this.state.textValue.length
      inputLength = valueLength * 20;
      return inputLength
    } else {
      var placeholderLength = this.props.placeholder.length
      inputLength = placeholderLength * 20;
      return inputLength
    }
  }

  onChange(event){
    this.setState({textValue:event.target.value })
    this.setState({width:this.calcInputLength()})
  }

  render () {
    return <Input type={this.props.type} value={this.state.textValue} style={{width: this.state.width}} onChange={this.onChange.bind(this)} placeholder={this.props.placeholder} />
  }
}

export default DynamicInput

const Input = styled.input`
fontSize: inherit;
color: red;
fontStyle: italic;
border: none;
outline: none;
`
