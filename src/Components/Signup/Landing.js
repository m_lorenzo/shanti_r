import React from 'react';

import FacebookLogin from 'react-facebook-login';
import styled from 'styled-components';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actionCreators from '../../actions/index'

class Landing extends React.Component {


  constructor(){
    super()
  }

  submit(){
    let user = {
      username: this.username.value,
      password: this.password.value
    }
    this.props.makeSignup(user, "userCredentials")
  }




  render () {

      return (
        <Wrapper>
          <MainDiv>
            <ChildDiv width={40} fontSize={1.1}>
              <h2><b>Signup for free and Shanti!</b></h2>
                <FacebookLogin
                    appId="1686455324988835"
                    autoLoad={false}
                    fields="id,first_name, last_name,picture,email,albums,gender,birthday,sports,work,education,photos,music,likes,location"
                    callback={function(response){
                      this.props.makeSignup(response, "basicInfo")
                    }.bind(this)
                    }
                    cssClass="fb-button"
                    scope="public_profile,email,user_birthday,user_actions.music,user_photos,user_likes,user_tagged_places, user_location, user_education_history, user_work_history"
                    textButton="Sign up with Facebook"/>
                  <input style={inputStyle} ref={(input) => this.username = input} placeholder="username" type="text"/>
                  <input style={inputStyle} ref={(input) => this.password = input} placeholder="password" type="password"/>
                  {/*}<Button onClick={function(){this.props.makeSignup({}, "basicInfo")}.bind(this)}>Signup with email</Button>*/}
                  <Button onClick={this.submit.bind(this)}>Signup with email</Button>
            </ChildDiv>
          </MainDiv>
          <MainDiv>
            <ChildDiv width={45} fontSize={1.5}>
              <h1><b>Here's how Shanti works</b></h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce eget lectus interdum, congue justo eu, molestie nisi. Morbi urna enim, luctus in nibh sed, semper ultrices nibh. Aliquam a tempor magna.</p>
            </ChildDiv>
          </MainDiv>
          <MainDiv>
            <ChildDiv width={90} flexDir={'row'}>
              <InfoDiv title={"WORD"}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin orci libero, interdum eu neque vel, tempus semper diam.
                Integer scelerisque mauris ipsum, at mollis velit facilisis at. Proin tempor odio.</InfoDiv>
              <InfoDiv title={"SEARCH"}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin orci libero, interdum eu neque vel, tempus semper diam.
                Integer scelerisque mauris ipsum, at mollis velit facilisis at. Proin tempor odio.</InfoDiv>
              <InfoDiv title={"ENJOY"}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin orci libero, interdum eu neque vel, tempus semper diam.
                Integer scelerisque mauris ipsum, at mollis velit facilisis at. Proin tempor odio.</InfoDiv>
            </ChildDiv>
          </MainDiv>
          <MainDiv height={50}>
            <ChildDiv>
              <h2>See how it works in 2 minutes</h2>
                <iframe width="520" height="315"
                  src="https://www.youtube.com/embed/EYmAfuWt5uw">
                </iframe>
            </ChildDiv>
          </MainDiv>
        </Wrapper>
      )
  }
}

function mapStateToProps(state){
  return {}
}


function mapDispachToProps(dispatch){
  return bindActionCreators({ ...actionCreators}, dispatch)
}


export default connect(mapStateToProps, mapDispachToProps)(Landing);



const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
`

const MainDiv = styled.div`
  width: 100%
  display: flex;
  justify-content: center;
  margin-bottom: 3%;
  flex-direction: column
  align-items: center;
`

const inputStyle = {
fontSize: "inherit",
outline: "none",
width: "280px",
bordeRadius: "5px",
border: ".5px solid black",
padding: "10px 30px",
textAlign: "center",
}

const ChildDiv = styled.div`
  width: ${(props)=> props.width}%;
  display: flex;
  justify-content: space-around;
  flex-direction: ${(props)=> props.flexDir || "column"};
  align-items: center;
  text-align: center;
  font-size: ${(props)=> props.fontSize || 1}em;
`

const Button = styled.button`
  font-size: inherit;
  width: 340px;
  border-radius: 5px;
  border: 1px solid black;
  background-color: ${(props)=> props.color || "grey" };
  padding: 10px 30px;
  margin-bottom: 25px;
`

const Div = styled.div`
  width: 20%;
`

const InfoDiv = (props)=> {
  return (
  <Div>
    <div>
      <span></span>
      <p style={{fontSize:"1.2em"}}>{props.title}</p>
    </div>
    <p style={{fontSize:".8em"}}>{props.children}</p>
  </Div>
  )
}
