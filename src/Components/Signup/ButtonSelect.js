import React from 'react'

import styled from 'styled-components';
import axios from 'axios'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actionCreators from '../../actions/index'


class ButtonSelect extends React.Component {

  constructor(props){
    super(props)
    this.state = {}
    this.infoSubject = props.infoSubject
    this[this.infoSubject] = []
  }

  setAttributes(){
    axios.get("http://localhost:3000/api/attributes"
    )
    .then((response) => {
       this.setState({attributes: response.data});
     })
     .catch(function (error) {
       console.log(error);
     });

  }

  componentWillMount(){
    this.setAttributes()
  }

  createButtons(){
    var self = this;
    return this.state.attributes[this.infoSubject].map(function(infoElement, index){
        return <Button onClick={self.addInfo.bind(self)} value={infoElement.name} key={index}>{infoElement.name}</Button>
    })
  }

  addInfo(event){
    this[this.infoSubject].push(event.currentTarget.value)
  }

  submit(){
    this.props.makeSignup(this[this.infoSubject], this.infoSubject)
  }

  render () {
    if (this.state.attributes) {
      return(
        <Wrapper>
          <MainDiv marginBottom={5}><h2>Hello , please choose your favourite {this.infoSubject}</h2></MainDiv>
          <MainDiv>
            {this.createButtons()}
          </MainDiv>
          <Button onClick={this.submit.bind(this)}>Next</Button>
        </Wrapper>
      )
    } else {
      return null
    }

  }
}



const HOC = (InnerComponent, infoSubject, nextPath)=> class extends React.Component{
  render(){
    return <InnerComponent {...this.props} infoSubject={infoSubject} nextPath={nextPath}/>
  }
}


function mapStateToProps(state){
  console.log(state)
  return {}
}


function mapDispachToProps(dispatch){
  return bindActionCreators({ ...actionCreators}, dispatch)
}



export const Music = connect(mapStateToProps, mapDispachToProps)(HOC(ButtonSelect, "music"))

export const Sports = connect(mapStateToProps, mapDispachToProps)(HOC(ButtonSelect, "sports"))

export const Cuisines = connect(mapStateToProps, mapDispachToProps)(HOC(ButtonSelect, "cuisines"))

export const Interests = connect(mapStateToProps, mapDispachToProps)(HOC(ButtonSelect, "interests"))

export const TravelTypes = connect(mapStateToProps, mapDispachToProps)(HOC(ButtonSelect, "travelTypes"))




const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`

const MainDiv = styled.div`
  width: 70%;
  margin-bottom: ${(props) => props.marginBottom || 0}%
  text-align: center;
`

const Button = styled.button`
  font-size: inherit;
  border-radius: 30px;
  border: 1px solid black;
  background-color: ${(props)=> props.color || "grey" };
  padding: 10px 30px;
  margin-bottom: 25px;
`
