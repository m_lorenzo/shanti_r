export function makeSignup(info, key){
  return {
    type: "NEXT_STEP",
    payload: info,
    key: key
  }
}
