import { combineReducers } from 'redux'
import { routerReducer as routing} from 'react-router-redux'

const initialState = {
  counter: {
    value: 0,
  }
};

function makeSignup(state = initialState, action){
  switch(action.type) {
    case 'NEXT_STEP' :
      return {
        counter: {
          value: state.counter.value + 1,
        },
        user: {
          ...state.user,
          [action.key]: action.payload,
        }
      }
      case 'PREVIOUS_STEP':
        return {...state,
        counter: {value: state.counter.value - 1}
        }
      default:
      return state;
  }
}


export const rootReducer = combineReducers({makeSignup, routing})
